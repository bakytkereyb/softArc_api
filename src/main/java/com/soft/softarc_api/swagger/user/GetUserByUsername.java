package com.soft.softarc_api.swagger.user;

import com.soft.softarc_api.mvc.entity.user.User;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Operation(
        summary = "Get User by username",
        description = "Get User by username")
@ApiResponses(value = {
        @ApiResponse(
                responseCode = "200",
                description = "OK",
                content = {@Content(
                        mediaType = "application/json",
                        schema = @Schema(implementation = User.class))})
})
public @interface GetUserByUsername {

}
