package com.soft.softarc_api.swagger.auth;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Operation(
        summary = "Login",
        description = "Login with username and password")
@ApiResponses(value = {
        @ApiResponse(
                responseCode = "200",
                description = "OK",
                content = {@Content(
                        mediaType = "application/json",
                        schema = @Schema(implementation = Token.class))})
})
public @interface Login {
}

@Data
@AllArgsConstructor
class Token {
    private String access_token;
//    private Date expiration_date;
}
