package com.soft.softarc_api.config;

import com.soft.softarc_api.filter.JwtAuthFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class SecurityConfig {

    private final JwtAuthFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .csrf()
                .disable();
        http.httpBasic().disable();

        http
                .authorizeHttpRequests()
                .requestMatchers(
                        "/swagger-ui/**",
                        "/swagger-ui.html",
                        "/javainuse-openapi/**",
                        "/techgeeknext-openapi/**",
                        "/api-docs/**",
                        "/docs/**",
                        "/api/file/**",
                        "/ws/**",
                        "/chat/ws/**",
                        "/chat/**"
                        )
                    .permitAll();

        http
                .authorizeHttpRequests()
                .requestMatchers(
                        "/api/auth/**",
                        "/api/user/register/**"
                )
                .permitAll();

        http
                .authorizeHttpRequests()
                .requestMatchers(
                        "/api/user/**"
                )
                .hasAnyAuthority("user", "admin");

        http
                .authorizeHttpRequests()
                .requestMatchers(
                        "/api/admin/**"
                )
                .hasAnyAuthority("admin");

        http
                .authorizeHttpRequests()
                .anyRequest()
                    .authenticated();


        http
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authenticationProvider(authenticationProvider)
                    .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }
}
