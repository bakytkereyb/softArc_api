package com.soft.softarc_api;

import com.soft.softarc_api.constants.EnvironmentProperties;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SecurityScheme(
        type = SecuritySchemeType.APIKEY,
        name = "Authorization",
        in = SecuritySchemeIn.HEADER)
@OpenAPIDefinition(
        info = @Info(title = "Project Documentation", version = EnvironmentProperties.APP_VERSION),
        security = { @SecurityRequirement(name = "Authorization") },
        servers = {
                @Server(url = EnvironmentProperties.SERVER_URL, description = "Default Server URL")
        }
)
public class SoftArcApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SoftArcApiApplication.class, args);
    }

}
