package com.soft.softarc_api.mvc.controller;

import com.soft.softarc_api.mvc.entity.Category;
import com.soft.softarc_api.mvc.entity.Item;
import com.soft.softarc_api.mvc.service.CategoryService;
import com.soft.softarc_api.mvc.service.ItemService;
import com.soft.softarc_api.request.ListPagination.ListPagination;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/items")
@Slf4j
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Tag(name = "Item", description = "APIs for operation on Item")
public class ItemController {

    private final ItemService itemService;
    private final CategoryService categoryService;

    @Operation(summary = "Get all item")
    @GetMapping("")
    @Transactional
    public ResponseEntity<ListPagination<Item>> getAll(
            @RequestParam("page") int page,
            @RequestParam("limit") int limit,
            @RequestParam(value = "categoryName", required = false) String categoryName
    ) {
        if (categoryName == null) {
            List<Item> items = itemService.listItem(page, limit);
            List<Item> nextItems = itemService.listItem(page + 1, limit);

            ListPagination<Item> listPagination = new ListPagination<>(items, !nextItems.isEmpty());
            return ResponseEntity.ok(listPagination);
        }

        Category category = categoryService.getCategoryByName(categoryName);
        if (category == null) {
            return ResponseEntity.badRequest().build();
        }

        List<Item> items = itemService.listItemByCategory(page, limit, category);
        List<Item> nextItems = itemService.listItemByCategory(page + 1, limit, category);

        ListPagination<Item> listPagination = new ListPagination<>(items, !nextItems.isEmpty());
        return ResponseEntity.ok(listPagination);
    }

    @Operation(summary = "Get item by id")
    @GetMapping("{id}")
    @Transactional
    public ResponseEntity<Item> getById(@PathVariable UUID id) {
        return ResponseEntity.ok(itemService.getItemById(id));
    }

    @Operation(summary = "Create new item")
    @PostMapping("")
    @Transactional
    public ResponseEntity<Item> create(
            @RequestParam("name") String name,
            @RequestParam("description") String description,
            @RequestParam("price") float price,
            @RequestParam("categoryId") UUID categoryId
    ) {
        Category category = categoryService.getCategoryById(categoryId);
        if (category == null) {
            return ResponseEntity.badRequest().build();
        }
        Item item = new Item(name, description, price, category);
        return ResponseEntity.ok(itemService.addItem(item));
    }

    @Operation(summary = "Delete item")
    @DeleteMapping("{id}")
    @Transactional
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        itemService.deleteItem(id);
        return ResponseEntity.ok().build();
    }
}