package com.soft.softarc_api.mvc.controller.storage;

import com.soft.softarc_api.mvc.service.file.FileService;
import com.soft.softarc_api.mvc.service.storage.FilesStorageService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/file")
@Slf4j
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Tag(name = "Storage", description = "Storage controller")
public class StorageController {
    private final FilesStorageService filesStorageService;
    private final FileService fileService;

//    @PostMapping(value = "/upload", consumes = {
//            "multipart/form-data"
//    })
//    public ResponseEntity uploadFileToStorage(@RequestParam("file") MultipartFile file) {
//        try {
//            String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));
//
//            File resultFile = new File(file.getOriginalFilename(), fileExtension);
//            resultFile = fileService.saveFile(resultFile);
//            resultFile.setFileName(resultFile.getId() + fileExtension);
//            resultFile = fileService.saveFile(resultFile);
//
//            String fileName = resultFile.getId() + fileExtension;
//            filesStorageService.save(file, fileName);
//
//            return ResponseEntity.ok(resultFile);
//        } catch (Exception e) {
//            log.error(e.toString());
//            return ResponseEntity.badRequest().body("Error: " + e.getMessage());
//        }
//    }

    @GetMapping("/get/{filename:.+}")
    @ResponseBody
    public ResponseEntity getFile(@PathVariable String filename) {
//        try {
            Resource file = filesStorageService.load(filename);
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getFilename());
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");

            return ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(file);

//            return ResponseEntity.ok()
//                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
//        } catch (Exception e) {
//            log.error(e.toString());
//            return ResponseEntity.badRequest().body("Error: " + e.getMessage());
//        }
    }
}
