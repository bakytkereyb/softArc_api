package com.soft.softarc_api.mvc.controller.user;

import com.soft.softarc_api.mvc.entity.user.Role;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.service.jwt.JwtService;
import com.soft.softarc_api.mvc.service.user.RoleService;
import com.soft.softarc_api.mvc.service.user.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
@Slf4j
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Tag(name = "User", description = "APIs for operation on User")
public class UserController {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;
    private final JwtService jwtService;

    private String getUsernameFromToken(String token) {
        return jwtService.extractUsername(token);
    }

//    @PatchMapping("/{id}/role/add")
//    @Transactional
//    public ResponseEntity addRoleToUser(@PathVariable UUID id,
//                                        @RequestParam("roleName") String roleName) {
//        User user = userService.getUserById(id);
//        if (user == null) {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
//        }
//        Role userRole = roleService.getRoleByName(roleName);
//        if (userRole == null) {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("role not found");
//        }
//        user.addRole(userRole);
//        user = userService.updateUser(user);
//        return ResponseEntity.ok(user);
//    }
//
//    @GetUserByUsername
//    @GetMapping("/get/{username}")
//    public ResponseEntity getUserByUsername(@PathVariable String username) {
//        User user = userService.getUserByUsername(username);
//        if (user == null) {
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
//        }
//        return ResponseEntity.ok(user);
//    }
//
//    @GetMapping("/get")
//    public ResponseEntity getAllUsers(@RequestParam("page") Integer page,
//                                      @RequestParam("limit") Integer limit) {
//        List<User> users = userService.getAllUsers(page, limit);
//        List<User> nextUsers = userService.getAllUsers(page + 1, limit);
//
//        ListPagination<User> listPagination = new ListPagination<>(users, !nextUsers.isEmpty());
//
//        return ResponseEntity.ok(listPagination);
//    }

    @PostMapping("/get/token")
    public ResponseEntity getUserByToken(@RequestParam("token") String token) {
        String username = jwtService.extractUsername(token);
        User user = userService.getUserByUsername(username);
        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
        }
        return ResponseEntity.ok(user);
    }

    @PostMapping("/register")
    @Transactional
    public ResponseEntity registerUser(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    ) throws Exception {
        Role userRole = roleService.getRoleByName("user");
        if (userRole == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user role not found");
        }

        User user = new User(username, password);
        user.addRole(userRole);

        user = userService.saveUser(user);

        return ResponseEntity.ok(user);
    }
}
