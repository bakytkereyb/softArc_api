package com.soft.softarc_api.mvc.controller.auth;


import com.soft.softarc_api.mvc.entity.Session;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.service.SessionService;
import com.soft.softarc_api.mvc.service.jwt.JwtService;
import com.soft.softarc_api.mvc.service.user.UserService;
import com.soft.softarc_api.swagger.auth.Login;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
@RequestMapping("/api/auth")
@RequiredArgsConstructor
@Tag(name = "Auth", description = "APIs for operation on Auth")
public class AuthController {
    private final UserService userService;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;
    private final SessionService sessionService;

    @PostMapping("/login")
    @Login
    @Transactional
    public ResponseEntity login(@RequestParam("username") String username,
                                @RequestParam("password") String password) {
//        try {
            log.info("username:" +  username);
            log.info("password:" +  password);
//            authenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(
//                            username,
//                            password
//                    )
//            );
            User user = userService.getUserByUsername(username);
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("user not found");
            }

            if (passwordEncoder.matches(password, user.getPassword())) {
                Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
                user.getRoles().forEach(role -> {authorities.add(new SimpleGrantedAuthority(role.getRoleName()));});
                UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);

                String jwtToken = jwtService.generateToken(userDetails);

                Session session = sessionService.getSessionByUser(user);
                if (session == null) {
                    session = new Session(user, jwtToken);
                } else {
                    session.setToken(jwtToken);
                }
                session = sessionService.saveSession(session);

                Token token = Token.builder()
                        .access_token(jwtToken)
    //                        .expiration_date(EnvironmentProperties.getJwtTokenExpirationDate())
                        .build();
                return ResponseEntity.ok(token);
            }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("password is incorrect");
//        } catch (Exception e) {
//            log.error(e.toString());
//            return ResponseEntity.badRequest().body("bad request");
//        }
    }
}

@Data
@AllArgsConstructor
@Builder
class Token {
    private String access_token;
//    private Date expiration_date;
}
