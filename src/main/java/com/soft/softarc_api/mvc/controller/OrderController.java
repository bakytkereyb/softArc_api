package com.soft.softarc_api.mvc.controller;

import com.soft.softarc_api.mvc.entity.Item;
import com.soft.softarc_api.mvc.entity.Order;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.service.ItemService;
import com.soft.softarc_api.mvc.service.OrderService;
import com.soft.softarc_api.mvc.service.jwt.JwtService;
import com.soft.softarc_api.mvc.service.user.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/orders")
@Slf4j
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Tag(name = "Order", description = "APIs for operation on Order")
public class OrderController {

    private final OrderService orderService;
    private final JwtService jwtService;
    private final UserService userService;
    private final ItemService itemService;

    private String getUsernameFromToken(String token) {
        return jwtService.extractUsername(token);
    }

    @GetMapping("/get/received")
    @Transactional
    public ResponseEntity<List<Order>> getReceived(
            @RequestParam("token") String token
    ) {
        User user = userService.getUserByUsername(getUsernameFromToken(token));
        if (user == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(orderService.findOrdersByToUser(user));
    }

    @GetMapping("/get/sent")
    @Transactional
    public ResponseEntity<List<Order>> getSent(
            @RequestParam("token") String token
    ) {
        User user = userService.getUserByUsername(getUsernameFromToken(token));
        if (user == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(orderService.findOrdersByFromUser(user));
    }

    @PostMapping("/create")
    @Transactional
    public ResponseEntity<Order> create(
            @RequestParam("token") String token,
            @RequestParam("toUsername") String toUsername,
            @RequestParam("itemsId") UUID[] itemsId
    ) {
        User fromUser = userService.getUserByUsername(getUsernameFromToken(token));
        if (fromUser == null) {
            return ResponseEntity.badRequest().build();
        }
        User toUser = userService.getUserByUsername(toUsername);
        if (toUser == null) {
            return ResponseEntity.badRequest().build();
        }

        float totalPrice = 0.0f;

        List<Item> items = new ArrayList<>();
        for (UUID itemId : itemsId) {
            Item item = itemService.getItemById(itemId);
            if (item == null) {
                return ResponseEntity.badRequest().build();
            }
            items.add(item);
            totalPrice += item.getPrice();
        }

        Order order = new Order(totalPrice, toUser, fromUser, items);
        return ResponseEntity.ok(orderService.save(order));
    }
}