package com.soft.softarc_api.mvc.controller;

import com.soft.softarc_api.mvc.entity.Category;
import com.soft.softarc_api.mvc.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/categories")
@Slf4j
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
@Tag(name = "Category", description = "APIs for operation on Category")
public class CategoryController {

    private final CategoryService categoryService;

    @Operation(summary = "Get all category")
    @GetMapping("")
    @Transactional
    public ResponseEntity<List<Category>> getAll(
    ) {
        return ResponseEntity.ok(categoryService.listCategories());
    }

    @Operation(summary = "Get category by id")
    @GetMapping("{id}")
    @Transactional
    public ResponseEntity<Category> getById(@PathVariable UUID id) {
        return ResponseEntity.ok(categoryService.getCategoryById(id));
    }

    @Operation(summary = "Create new category")
    @PostMapping("")
    @Transactional
    public ResponseEntity<Category> create(@RequestParam("categoryName") String categoryName) {

        if (categoryService.getCategoryByName(categoryName) != null) {
            return ResponseEntity.badRequest().build();
        }

        Category category = new Category(categoryName);

        return ResponseEntity.ok(categoryService.addCategory(category));
    }

//    @Operation(summary = "Update category")
//    @PutMapping("{id}")
//    public ResponseEntity<Category> update(@PathVariable UUID id, @RequestBody Category categoryDtoRequest) throws ChangeSetPersister.NotFoundException {
//        return ResponseEntity.ok(categoryService.update(id, categoryDtoRequest));
//    }

    @Operation(summary = "Delete category")
    @DeleteMapping("{id}")
    @Transactional
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        categoryService.delete(id);
        return ResponseEntity.ok().build();
    }
}
