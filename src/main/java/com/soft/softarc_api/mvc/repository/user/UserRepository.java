package com.soft.softarc_api.mvc.repository.user;


import com.soft.softarc_api.mvc.entity.user.Role;
import com.soft.softarc_api.mvc.entity.user.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, UUID>, JpaRepository<User, UUID> {
    Optional<User> findByUsername(String username);
//    List<User> findAllByRolesContaining(Role role, Pageable pageable);
    List<User> findAllByRolesContaining(Role role, Pageable pageable);
    long countByRolesContaining(Role role);
//    List<User> findAll(Pageable pageable);
}
