package com.soft.softarc_api.mvc.repository;

import com.soft.softarc_api.mvc.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category, UUID>, JpaRepository<Category, UUID> {
    Optional<Category> findByCategoryName(String categoryName);
}