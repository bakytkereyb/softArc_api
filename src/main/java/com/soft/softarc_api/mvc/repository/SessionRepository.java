package com.soft.softarc_api.mvc.repository;

import com.soft.softarc_api.mvc.entity.Session;
import com.soft.softarc_api.mvc.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface SessionRepository extends PagingAndSortingRepository<Session, UUID>, JpaRepository<Session, UUID> {
    Optional<Session> findByUser(User user);
}
