package com.soft.softarc_api.mvc.repository.file;


import com.soft.softarc_api.mvc.entity.file.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface FileRepository extends JpaRepository<File, UUID>, PagingAndSortingRepository<File, UUID> {
}
