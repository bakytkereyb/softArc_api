package com.soft.softarc_api.mvc.repository;

import com.soft.softarc_api.mvc.entity.Item;
import com.soft.softarc_api.mvc.entity.Order;
import com.soft.softarc_api.mvc.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<Order, UUID>, JpaRepository<Order, UUID> {
    List<Order> findByToUser(User user);
    List<Order> findByFromUser(User user);
}
