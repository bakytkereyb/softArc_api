package com.soft.softarc_api.mvc.repository;

import com.soft.softarc_api.mvc.entity.Category;
import com.soft.softarc_api.mvc.entity.Item;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ItemRepository extends PagingAndSortingRepository<Item, UUID>, JpaRepository<Item, UUID> {
    public List<Item> findByCategory(Category category, Pageable pageable);

}
