package com.soft.softarc_api.mvc.entity;

import com.soft.softarc_api.annotaitons.TrimmedString;
import com.soft.softarc_api.mvc.entity.user.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "orders")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    private float totalPrice;

    @ManyToOne
    private User toUser;

    @ManyToOne
    private User fromUser;

    @ManyToMany
    private List<Item> items = new ArrayList<>();

    public Order(float totalPrice, User toUser, User fromUser, List<Item> items) {
        this.totalPrice = totalPrice;
        this.toUser = toUser;
        this.fromUser = fromUser;
        this.items = items;
    }

    public void addItem(Item item) {
        if (!items.contains(item)) {
            items.add(item);
        }
    }

    public void removeItem(Item item) {
        items.remove(item);
    }
}