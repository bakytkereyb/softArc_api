package com.soft.softarc_api.mvc.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.soft.softarc_api.annotaitons.TrimmedString;
import com.soft.softarc_api.annotaitons.TrimmedStringListener;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "category")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(TrimmedStringListener.class)
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    public UUID id;

    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @TrimmedString
    @Column(columnDefinition = "TEXT")
    public String categoryName;

    public Category(String categoryName) {
        this.categoryName = categoryName;
    }

    //    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//    public List<Item> itemList;
}
