package com.soft.softarc_api.mvc.entity.file;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.soft.softarc_api.annotaitons.TrimmedStringListener;
import com.soft.softarc_api.mvc.entity.user.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "files")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(TrimmedStringListener.class)
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(columnDefinition = "TEXT")
    private String fileName;

    @Column(columnDefinition = "TEXT")
    private String label;

    @Column(columnDefinition = "TEXT")
    private String extension;

    @ManyToOne
    @JsonIgnore
    private User user;

    public File(String label, String extension, User user) {
        this.label = label;
        this.extension = extension;
    }
}
