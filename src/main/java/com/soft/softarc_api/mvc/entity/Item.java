package com.soft.softarc_api.mvc.entity;

import com.soft.softarc_api.annotaitons.TrimmedString;
import com.soft.softarc_api.annotaitons.TrimmedStringListener;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "item")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(TrimmedStringListener.class)
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @TrimmedString
    @Column(columnDefinition = "TEXT")
    private String name;

    @TrimmedString
    @Column(columnDefinition = "TEXT")
    private String description;

    @Column
    private float price;

    @ManyToOne
//    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    public Item(String name, String description, float price, Category category) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
    }
}
