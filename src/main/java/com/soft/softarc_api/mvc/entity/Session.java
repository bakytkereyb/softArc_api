package com.soft.softarc_api.mvc.entity;

import com.soft.softarc_api.annotaitons.TrimmedStringListener;
import com.soft.softarc_api.mvc.entity.user.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "sessions")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(TrimmedStringListener.class)
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @CreationTimestamp
    private LocalDateTime createdAt;
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @ManyToOne
    private User user;

    @Column(columnDefinition = "TEXT")
    private String token;

    public Session(User user, String token) {
        this.user = user;
        this.token = token;
    }
}
