package com.soft.softarc_api.mvc.service.user;

import com.soft.softarc_api.constants.EnvironmentProperties;
import com.soft.softarc_api.mvc.entity.user.Role;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.repository.user.RoleRepository;
import com.soft.softarc_api.mvc.repository.user.UserRepository;
import com.soft.softarc_api.mvc.service.jwt.JwtService;
import com.soft.softarc_api.validation.PasswordConstraintValidator;
import com.soft.softarc_api.validation.UsernameValidator;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserService {
//public class UserService {
    private final String baseURL = EnvironmentProperties.SERVER_URL;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

//    private final JavaMailSender mailSender;

    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    public User saveUser(User user) throws Exception {
        User existingUser = userRepository.findByUsername(user.getUsername()).orElse(null);
        if (existingUser == null) {
            if (!UsernameValidator.isValid(user.getUsername())) {
                throw new Exception("username is not valid");
            }
            if (!PasswordConstraintValidator.isValid(user.getPassword())) {
                throw new Exception("password is not valid");
            }
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            return userRepository.save(user);
        }
        throw new Exception("User already exists");
    }

    public long countAllUsers() {
        return userRepository.count();
    }

    public long countUsersByRole(Role role) {
        return userRepository.countByRolesContaining(role);
    }

    public User updateUser(User user) {
        return userRepository.save(user);
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }

    public User getUserById(UUID id) {
        return userRepository.findById(id).orElse(null);
    }

    public List<User> getAllUsers(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit, Sort.by("createdAt").ascending());
        return userRepository.findAll(pageable).getContent();
    }

    public List<User> getAllUsersByRolePageable(int page, int limit, Role role) {
        Pageable pageable = PageRequest.of(page, limit, Sort.by("createdAt").ascending());
        return userRepository.findAllByRolesContaining(role, pageable);
    }
}
