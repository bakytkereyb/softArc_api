package com.soft.softarc_api.mvc.service;

import com.soft.softarc_api.mvc.entity.Order;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.repository.OrderRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService{
    private final OrderRepository orderRepository;

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public List<Order> findOrdersByToUser(User user) {
        return orderRepository.findByToUser(user);
    }

    @Override
    public List<Order> findOrdersByFromUser(User user) {
        return orderRepository.findByFromUser(user);
    }

    @Override
    public Order findOrderById(UUID id) {
        return orderRepository.findById(id).orElse(null);
    }

}
