package com.soft.softarc_api.mvc.service.user;

import com.soft.softarc_api.mvc.entity.user.Role;
import com.soft.softarc_api.mvc.repository.user.RoleRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class RoleService {

    private final RoleRepository roleRepository;

    public Role saveRole(Role role) {
        Role existingRole = roleRepository.findByRoleName(role.getRoleName());
        if (existingRole == null) {
            return roleRepository.save(role);
        }
        return existingRole;
    }

    public Role getRoleByName(String roleName) {
        return roleRepository.findByRoleName(roleName);
    }
}
