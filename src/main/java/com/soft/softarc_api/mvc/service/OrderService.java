package com.soft.softarc_api.mvc.service;

import com.soft.softarc_api.mvc.entity.Order;
import com.soft.softarc_api.mvc.entity.user.User;

import java.util.List;
import java.util.UUID;

public interface OrderService {

    Order save(Order order);
    List<Order> findOrdersByToUser(User user);
    List<Order> findOrdersByFromUser(User user);
    Order findOrderById(UUID id);
}
