package com.soft.softarc_api.mvc.service.storage;

import com.soft.softarc_api.mvc.entity.file.File;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.service.file.FileService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class StorageService {
    private final FilesStorageService filesStorageService;
    private final FileService fileService;

    public File uploadFileToStorage(MultipartFile file, User user) {
        String fileExtension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));

        File resultFile = new File(file.getOriginalFilename(), fileExtension, user);
        resultFile = fileService.saveFile(resultFile);
        resultFile.setFileName(resultFile.getId() + fileExtension);
        resultFile = fileService.saveFile(resultFile);

        String fileName = resultFile.getId() + fileExtension;
        filesStorageService.save(file, fileName);

        return resultFile;
    }

    public File uploadImageFileToStorage(byte[] buffer, String fileExtension, User user) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer);
        File resultFile = new File("questionImage", fileExtension, user);
        resultFile = fileService.saveFile(resultFile);
        resultFile.setFileName(resultFile.getId() + fileExtension);
        resultFile = fileService.saveFile(resultFile);

        String fileName = resultFile.getId() + fileExtension;

        filesStorageService.save(byteArrayInputStream, fileName);
        return resultFile;
    }

    public void deleteFile(File file) {
        fileService.deleteFile(file);
        filesStorageService.deleteFileByName(file.getFileName());
    }
}
