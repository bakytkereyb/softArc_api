package com.soft.softarc_api.mvc.service;

import com.soft.softarc_api.mvc.entity.Category;
import com.soft.softarc_api.mvc.entity.Item;
import com.soft.softarc_api.mvc.repository.ItemRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class ItemServiceImpl implements ItemService{

    private final ItemRepository itemRepository;

    @Override
    public Item addItem(Item item) {
        return itemRepository.save(item);
    }

    @Override
    public List<Item> listItem(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit, Sort.by("createdAt").ascending());
        return itemRepository.findAll(pageable).getContent();
    }

    @Override
    public List<Item> listItemByCategory(int page, int limit, Category category) {
        Pageable pageable = PageRequest.of(page, limit, Sort.by("createdAt").ascending());
        return itemRepository.findByCategory(category, pageable);
    }

    @Override
    public Item getItemById(UUID itemId) {
        return itemRepository.findById(itemId).orElse(null);
    }

//    @Override
//    public List<Item> findByCategory(UUID categoryId) {
//        return itemRepository.findByCategoryId(categoryId);
//    }

    @Override
    public void deleteItem(UUID itemId) {
        itemRepository.deleteById(itemId);
    }
}