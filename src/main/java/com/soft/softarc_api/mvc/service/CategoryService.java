package com.soft.softarc_api.mvc.service;

import com.soft.softarc_api.mvc.entity.Category;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CategoryService {

    public Category addCategory(Category category);

    public List<Category> listCategories();

    public void delete(UUID categoryId);

    public Category update(UUID id, Category category) throws ChangeSetPersister.NotFoundException;

    public Category getCategoryById(UUID categoryId);
    public Category getCategoryByName(String categoryName);

}
