package com.soft.softarc_api.mvc.service;

import com.soft.softarc_api.mvc.entity.Session;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.repository.SessionRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class SessionService {
    private final SessionRepository sessionRepository;

    public Session saveSession(Session session) {
        return sessionRepository.save(session);
    }

    public Session getSessionByUser(User user) {
        return sessionRepository.findByUser(user).orElse(null);
    }
}
