package com.soft.softarc_api.mvc.service;

import com.soft.softarc_api.mvc.entity.Category;
import com.soft.softarc_api.mvc.entity.Item;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ItemService {

    public Item addItem(Item item);

    public List<Item> listItem(int page, int limit);
    public List<Item> listItemByCategory(int page, int limit, Category category);

    public Item getItemById(UUID itemId);

//    public List<Item> findByCategory(UUID itemId);

    public void deleteItem(UUID itemId);

}
