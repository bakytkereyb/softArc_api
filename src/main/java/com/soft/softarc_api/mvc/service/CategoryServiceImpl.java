package com.soft.softarc_api.mvc.service;

import com.soft.softarc_api.mvc.entity.Category;
import com.soft.softarc_api.mvc.repository.CategoryRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CategoryServiceImpl implements CategoryService{

    private final CategoryRepository categoryRepository;

    @Override
    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public List<Category> listCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public void delete(UUID categoryId) {
        categoryRepository.deleteById(categoryId);
    }

    @Override
    public Category update(UUID id, Category category) throws ChangeSetPersister.NotFoundException {
        Category categoryFromDb = categoryRepository.findById(id).orElseThrow(ChangeSetPersister.NotFoundException::new);

        categoryFromDb.setCategoryName(
                category.categoryName
        );

        return categoryRepository.save(
                categoryFromDb
        );
    }

    @Override
    public Category getCategoryById(UUID categoryId) {
        return categoryRepository.findById(categoryId).orElse(null);
    }

    @Override
    public Category getCategoryByName(String categoryName) {
        return categoryRepository.findByCategoryName(categoryName).orElse(null);
    }

}
