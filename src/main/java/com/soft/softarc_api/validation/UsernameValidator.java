package com.soft.softarc_api.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsernameValidator implements ConstraintValidator<ValidUsername, String> {
    private static final String USERNAME_PATTERN =
            "^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$";
    private static final Pattern PATTERN = Pattern.compile(USERNAME_PATTERN);

    @Override
    public boolean isValid(final String username, final ConstraintValidatorContext context) {
        if (username == null) {
            return false;
        }
        if (username.isBlank()) {
            return false;
        }
        if (username.contains(" ")) {
            return false;
        }
        if (username.isEmpty()) {
            return false;
        }
        if (username.length() < 5 || username.length() > 20) {
            return false;
        }
        Matcher matcher = PATTERN.matcher(username);
        return matcher.matches();
    }

    public static boolean isValid(final String username) {
        if (username == null) {
            return false;
        }
        if (username.isBlank()) {
            return false;
        }
        if (username.contains(" ")) {
            return false;
        }
        if (username.isEmpty()) {
            return false;
        }
        if (username.length() < 5 || username.length() > 20) {
            return false;
        }
        Matcher matcher = PATTERN.matcher(username);
        return matcher.matches();
    }

    // Username consists of alphanumeric characters (a-zA-Z0-9), lowercase, or uppercase.
    // Username allowed of the dot (.), underscore (_), and hyphen (-).
    // The dot (.), underscore (_), or hyphen (-) must not be the first or last character.
    // The dot (.), underscore (_), or hyphen (-) does not appear consecutively, e.g., java..regex
    // The number of characters must be between 5 to 20.
}
