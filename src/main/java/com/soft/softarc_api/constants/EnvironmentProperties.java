package com.soft.softarc_api.constants;

public class EnvironmentProperties {
    // public static final String SERVER_URL = "http://localhost:8080";
    public static final String SERVER_URL = "http://40.117.190.226";
    public static final String CLIENT_URL = "http://localhost:3000";
    public static final String SERVER_URL_WWW = "http://www.40.117.190.226";
    public static final String APP_VERSION = "1.0.0";
    public static final String MAIN_MAIL = "bakytkerey.b@gmail.com";

//    public static Date getJwtTokenExpirationDate() {
//        return new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 3);
//        // 1,440,000 milliseconds (1000 milliseconds * 60 seconds * 60 minutes * 3) = 60 min * 3 = 3 hours
//    }
}
