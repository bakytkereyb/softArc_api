package com.soft.softarc_api.init;

import com.soft.softarc_api.mvc.entity.user.Role;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.service.storage.FilesStorageService;
import com.soft.softarc_api.mvc.service.user.RoleService;
import com.soft.softarc_api.mvc.service.user.UserService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
@Transactional
public class InitService {
    private final UserService userService;
    private final RoleService roleService;
    private final FilesStorageService filesStorageService;

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        log.warn("INITIALIZING...");

        log.warn("INITIALIZING... - file storage");
        filesStorageService.init();

        Role adminRole = new Role("admin");
        Role userRole = new Role("user");

        log.warn("INITIALIZING... - roles");
        adminRole = roleService.saveRole(adminRole);
        userRole = roleService.saveRole(userRole);

        User admin = new User(
                "admin",
                "admin112233");


        log.warn("INITIALIZING... - users");
        try {
            admin.addRole(adminRole);
            admin = userService.saveUser(admin);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        log.info("INITIALIZING... - FINISHED");
    }
}
