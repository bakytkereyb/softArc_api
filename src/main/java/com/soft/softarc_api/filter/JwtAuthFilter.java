package com.soft.softarc_api.filter;

import com.soft.softarc_api.mvc.entity.Session;
import com.soft.softarc_api.mvc.entity.user.User;
import com.soft.softarc_api.mvc.service.SessionService;
import com.soft.softarc_api.mvc.service.jwt.JwtService;
import com.soft.softarc_api.mvc.service.user.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtAuthFilter extends OncePerRequestFilter {

    private final UserDetailsService userDetailsService;
    private final JwtService jwtService;
    private final UserService userService;
    private final SessionService sessionService;

    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain) throws ServletException, IOException {
        try {
            final String authHeader = request.getHeader("Authorization");
            final String username;
            final String jwtToken;

            if (authHeader == null /*|| !authHeader.startsWith("Bearer ") */) {
                filterChain.doFilter(request, response);
                return;
            }
            jwtToken = authHeader;
//            try {
//                jwtService.isTokenExpired(jwtToken);
//            } catch (Exception e) {
//                response.setStatus(401);
//                return;
//            }
            username = jwtService.extractUsername(jwtToken);
            User user = userService.getUserByUsername(username);
            if (user != null) {
                Session session = sessionService.getSessionByUser(user);
                if (session != null) {
                    if (!session.getToken().equals(jwtToken)) {
                        response.setStatus(401);
                        return;
                    }
                } else {
                    response.setStatus(401);
                    return;
                }
            }
            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                if (jwtService.isTokenValid(jwtToken, userDetails)) {
                    UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                            userDetails,
                            null,
                            userDetails.getAuthorities()
                    );
                    authToken.setDetails(
                            new WebAuthenticationDetailsSource().buildDetails(request)
                    );
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                }
            }
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            log.error(e.toString());
            response.setStatus(400);
        }

    }

//    private String getUsernameFromToken(String token) {
//        Algorithm algorithm = Algorithm.HMAC256(secretKey.getBytes());
//        JWTVerifier verifier = JWT.require(algorithm).build();
//        DecodedJWT decodedJWT = verifier.verify(token);
//        String username = decodedJWT.getSubject();
//        return username;
//    }
}
