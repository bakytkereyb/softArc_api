package com.soft.softarc_api.annotaitons;

import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

import java.lang.reflect.Field;

public class TrimmedStringListener {
    @PrePersist
    @PreUpdate
    public void trimStrings(Object entity) throws IllegalAccessException {
        Class<?> clazz = entity.getClass();

        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(TrimmedString.class) && field.getType().equals(String.class)) {
                field.setAccessible(true);
                String value = (String) field.get(entity);
                if (value != null) {
                    field.set(entity, value.trim());
                }
            }
        }
    }
}
