# FROM openjdk:17-oracle
# ARG JAR_FILE=target/*.jar
# COPY ./target/SoftArcApiApplication-0.0.1.jar app.jar
# ENTRYPOINT ["java", "-jar", "/app.jar"]

FROM maven:3.8.4-openjdk-17 as builder
WORKDIR /opt/app
COPY mvnw pom.xml .
RUN mvn dependency:go-offline
COPY src ./src
RUN mvn clean package -DskipTests

FROM eclipse-temurin:17-jre-alpine
WORKDIR /opt/app
EXPOSE 8181
COPY --from=builder /opt/app/target/*.jar /opt/app/*.jar
ENTRYPOINT ["java", "-jar", "/opt/app/*.jar"]
